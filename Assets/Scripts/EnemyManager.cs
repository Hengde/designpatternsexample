﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace gc{
	
	public class EnemyManager : MonoBehaviour {

		public EnemyWave [] waves;
		private List<EnemyMovement> enemies;
		private string [] enemyPrefabs;

		private delegate void StateUpdate();
		private StateUpdate stateUpdate;

		private int currentWave = 0;
		private float waveTimer;
		private int currentWavePoint=0; 


		// Use this for initialization
		void Start () {
			enemies = new List<EnemyMovement>();
			setUpEnemiesPrefabs();
			waveTimer = 0;
			stateUpdate = startState;
		}

		private void setUpEnemiesPrefabs(){
			enemyPrefabs = new string[2];
			enemyPrefabs[0] = "Prefabs/Enemyship";
			enemyPrefabs[1] = "Prefabs/InterceptorShip";
		}

		
		// Update is called once per frame
		void Update () {
			if(stateUpdate != null){
				stateUpdate();
			}
		}

		private void setState(WaveState ws){
			switch(ws){
			case(WaveState.Starting):
				stateUpdate = startState;
				break;
			case(WaveState.During):
				stateUpdate = duringState;
				break;
			case(WaveState.AboutToEnd):
				stateUpdate = aboutToEndState;
				break;
			case(WaveState.Cleared):
				stateUpdate = clearedState;
				break;
			case(WaveState.None):
				stateUpdate = duringState;
				break;
			default:
				break;
			}
					
		}

		public void AddEnemy(int n=900){
			if(n >enemies.Count-1){
				n = Mathf.FloorToInt(UnityEngine.Random.Range(0,enemyPrefabs.Length-.01f));
			}
			GameObject e = Spawner.Spawn(enemyPrefabs[n], Game.instance.RandomWorldPosition(3)) as GameObject;
			enemies.Add(e.GetComponent<EnemyMovement>());
		}

		public void DestroyEnemy(EnemyMovement em){
			EventManager.instance.QueueEvent(new EnemyDieEvent());
			//EventManager.instance.Raise(new EnemyDieEvent());
			enemies.Remove(em);
			Destroy(em.gameObject);
		}



		private void startState(){
			//display that the next state is beginning. maybe a countdown
			Game.instance.displayText("GET READY...");
			waveTimer = 0;
			currentWavePoint =0;
			Invoke("startWave",5f);

			
		}

		private void duringState(){
//			Debug.Log(waveTimer);
			//if the spawn time of the current wave's current point is greater than the timer, then spawn all of the enemies in that wavepoint
			if(currentWavePoint<waves[currentWave].wavePoints.Length){
				if(waveTimer > waves[currentWave].wavePoints[currentWavePoint].spawnTime){
//					Debug.Log(waveTimer + " " + waves[currentWave].wavePoints[currentWavePoint].spawnTime);
					for(int i = 0; i < waves[currentWave].wavePoints[currentWavePoint].enemy.Length; i++){
						AddEnemy(waves[currentWave].wavePoints[currentWavePoint].enemy[i]);
					}
					//increment current wave point
					currentWavePoint++;
				}
			} else {
				//out of wave points! 
				Debug.Log("WAVE OVER");
				setState(WaveState.AboutToEnd);
			}

//			for(int i=enemies.Count-1; i>=0;--i){
//				if (enemies[i].GetComponent<Enemy>().Health<=0){
//					EnemyMovement temp = enemies[i];
//					enemies.RemoveAt(i);
//					Destroy(temp);
//				}
//			}

			waveTimer += Time.deltaTime;


		}

		private void aboutToEndState(){
//			Debug.Log("ABOUT TO END");
			if(enemies.Count ==0){
				Debug.Log(currentWave);
				currentWave++;
				setState(WaveState.Cleared);
			}
		}


		private void clearedState(){
			//Debug.Log("CLEAR STATE" +currentWave+ " " +waves.Length);
			if(currentWave == waves.Length){
				//that was the last wave. End the game.
				string finalText = "YOU WIN! FINAL SCORE: "+ Game.instance.sm.score;
				if(Game.instance.sm.noDeaths){
					finalText += " **PERFECT** ";
				}
				Game.instance.displayText(finalText);
			
			} else {
				//start  the next wave
				Game.instance.displayText("WAVE CLEAR!");
				Invoke("prepareWave", 5f);
				stateUpdate = noneState;
			}
		}



		private void noneState(){

		}

		private void prepareWave(){
			setState (WaveState.Starting);
		}

		private void startWave(){
			Game.instance.displayText("");
			setState (WaveState.During);
		}


	}

	[System.Serializable]
	public class EnemyWave{
		[SerializeField]
		public EnemyWavePoint [] wavePoints;
	}
	[System.Serializable]
	public class EnemyWavePoint{
		[SerializeField]
		public int [] enemy;
		[SerializeField]
		public float spawnTime;
	}

	public enum WaveState{Starting,During,Cleared,AboutToEnd,None}

}

public class EnemyDieEvent : GameEvent{

}
