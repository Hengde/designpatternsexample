﻿
using UnityEngine;

namespace gc {

    public class EnemyMovement : MovementController {

		public float MaxSpeed;
		public AudioClip deathSound;
		protected float currentSpeed;

		protected Player player;
		protected GameObject playerObject;

		protected Vector2 playerPosition;
		protected Vector2 position;

		protected Vector2 offsetToPlayer;
		protected Vector2 directionToPlayer;

		protected Vector2 desiredVelocity;
		protected Vector2 steeringForce;

        protected override void FixedUpdate()
        {
			// get the player (if one can't be found bail out)
			updatePlayerPosition();

			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			doMovement();
        }

		protected virtual void updatePlayerPosition(){
			player = Player.Instance;
			if (player == null) {
				return;
			}

			playerObject = player.gameObject;

			// get the player's position and where it is in relation to this enemy
			playerPosition = playerObject.transform.position;
			position = gameObject.transform.position;

			offsetToPlayer = playerPosition - position;
			directionToPlayer = offsetToPlayer.normalized;
		}

		protected virtual void doMovement(){
			moveToTarget();

			// make the body face in the direction of movement
			rotateTowardsTarget(body.velocity.x, body.velocity.y);
		}

		protected virtual void moveToTarget(){
			desiredVelocity = directionToPlayer * MaxSpeed;
			steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, MaxSpeed);
			body.AddForce(steeringForce);
			currentSpeed = body.velocity.magnitude;
			if (currentSpeed > MaxSpeed) {
				body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
			}
		}

		protected virtual void rotateTowardsTarget(float x, float y){
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(y, x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);
		}
    }

}
