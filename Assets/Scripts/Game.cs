﻿using UnityEngine;
using UnityEngine.UI;

namespace gc {

	public class Game : MonoBehaviour {

		private static Game _instance;
		public static Game instance
		{
			get {
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<Game>();
					//				DontDestroyOnLoad(_instance.gameObject);
				}
				return _instance;
			}
		}

		public EnemyManager em;
		public ScoreManager sm;

		public Text display;


		void Update () {
			// Spawn player
			if (Player.Instance == null) {
				Spawner.Spawn("Prefabs/Playership", RandomWorldPosition(3));	
			}

			// Spawn enemies
//			if (Enemy.NumEnemies < MinNumEnemies) {
//				float rand = Random.Range(0,100);
//				if(rand <= 75){
//					Spawner.Spawn("Prefabs/Enemyship", RandomWorldPosition(3));
//				} else {
//					Spawner.Spawn("Prefabs/Interceptorship", RandomWorldPosition(3));
//				}
//
//			}

		}
			
		public Vector3 RandomWorldPosition(int gutter) {
			return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0); 
		}

		public void displayText(string text){
			if(display!=null){
				display.gameObject.SetActive(true);
				display.text = text;
			}
		}

	}

}

