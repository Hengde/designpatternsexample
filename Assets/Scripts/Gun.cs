using UnityEngine;

namespace gc {

	public class Gun : MonoBehaviour {

        private readonly static float _minOffset = 0.01f;

        Vector2 _firingDirection;
        public float FireInterval;
        private float _nextFireTime;
        private bool _isFiring = true;

        void Start() 
		{
             _firingDirection = Vector2.up;
        }

        public void StartFiring() 
		{
            _isFiring = true;
        }

		public void StopFiring() 
		{
            _isFiring = false;
        }

        void Update()
        {
            // Aim the gun wherever the mouse is pointed
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 shipPosition = gameObject.transform.position;
            Vector2 offsetToMouse = mousePosition - shipPosition;

            // Only update the firing direction when the mouse isn't directly on top of the ship
            // (this is so microscopic changes in mouse position don't change the firing direction)
            if (offsetToMouse.magnitude > _minOffset) 
			{
                Vector3 mouseDirection = offsetToMouse.normalized;
                _firingDirection = mouseDirection;
            }

            // Fire the gun
			if (_isFiring && Time.time >= _nextFireTime) 
			{
                Fire();
            }
        }

        void Fire()
        {
			Bullet.Fire(_firingDirection, gameObject.transform.position);
			_nextFireTime = Time.time + (FireInterval / 1000);
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_fire");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, 0.1f);
        }

    }

}
