﻿
using UnityEngine;

namespace gc {

	public class InterceptorMovement : EnemyMovement {


		private float countdownReset = 2;
		private float countdownTimer = 3;

		private bool isMoving = false;

		Vector2 targetPoint;

		protected override void moveToTarget(){
			//Debug.Log("doin it");
			transform.position = Vector3.MoveTowards(transform.position,targetPoint,MaxSpeed*Time.deltaTime);
		}

		protected override void doMovement(){
			if(isMoving){
//				Debug.Log(targetPoint);

				//we are currently moving towards target position
				desiredVelocity = directionToPlayer * MaxSpeed;
				if (currentSpeed > MaxSpeed) {
					body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
				}
				moveToTarget();
				//if we reach it, set ismoving false and reset timer
				if(position == targetPoint){
					isMoving = false;
					countdownTimer = countdownReset + Random.Range(-.75f, .75f);
				}
					
			} else {
				//keep setting our target while we're waiting to move again - halfway between player and mouse
				targetPoint = new Vector2((playerObject.transform.position.x + playerObject.GetComponent<PlayerMovement>().mousePosition.x) /2, (playerObject.transform.position.y + playerObject.GetComponent<PlayerMovement>().mousePosition.y) /2);

				//playerObject.transform.position + (Vector3)playerObject.GetComponent<PlayerMovement>().mousePosition /2;
				offsetToPlayer = targetPoint - position;
				directionToPlayer = offsetToPlayer.normalized;

				//rotate towards target
				rotateTowardsTarget(directionToPlayer.x, directionToPlayer.y);

				//countdown to move towards target position
				countdownTimer -= Time.deltaTime;

				//if countdown reaches 0, initiate movement
				if(countdownTimer <= 0){
//					Debug.Log(targetPoint);
					isMoving = true;
				//	moveToTarget();
				}

			}
		}
	}

}
