﻿using UnityEngine;

namespace gc {

	public class PlayerMovement : MovementController {

		public float MaxSpeed;

		public Vector2 forceDirection {get;private set;}
		public Vector3 shipPosition {get; private set;}
		public Vector3 mousePosition {get; private set;}

        override protected void FixedUpdate() {
                
			// Clear the velocity every frame so the player's ship responds instantly to changes in direction
			body.velocity = Vector2.zero;
			body.angularVelocity = 0;

			// determine the positions of the mouse and player
			mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            shipPosition = gameObject.transform.position;
            Vector3 offsetToMouse = mousePosition - shipPosition;

			// Don't bother updating the forces if the player is very close to the mouse
			float distanceToMouse = offsetToMouse.magnitude;
			if (distanceToMouse > Mathf.Epsilon) {

				// scale the speed so that the force gets smaller when the ship gets close to the mouse
				// NOTE: The scaling the distance by the inverse of the time step has to do with how Box2D handles forces vs. impulses.
				float speed = Mathf.Min(MaxSpeed, (distanceToMouse / Time.fixedDeltaTime));

				// calculate the direction of the force required to move the player towards the mouse
				forceDirection = offsetToMouse.normalized;
				Vector2 force = forceDirection * speed;

				// apply the force as an impulse (impulses are easier if you're constantly clearing the forces like we do here)
				body.AddForce(force, ForceMode2D.Impulse);
			}
        }
    }

}
