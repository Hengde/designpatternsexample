﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public int score {get; private set;}
	public bool noDeaths{get; private set;}

	void Awake(){
		EventManager.instance.AddListener<EnemyDieEvent>(IncreaseScore);
		EventManager.instance.AddListener<PlayerDiedEvent>(DecreaseScore);
	}

	// Use this for initialization
	void Start () {
		noDeaths = true;
	}
	
	// Update is called once per frame
	void Update () {
		EventManager.instance.ProcessQueuedEvents();
	}

	public void Die() {
		// make sure you *ALWAYS* unregister for events
		// otherwise you can generate memory leaks, or worse...
		EventManager.instance.RemoveListener<EnemyDieEvent>(IncreaseScore);	
		EventManager.instance.RemoveListener<PlayerDiedEvent>(DecreaseScore);
	}

	void IncreaseScore(GameEvent e){
		score++;
		Debug.Log(score);
	}

	void DecreaseScore(GameEvent e){
		noDeaths = false;
		score -=10;
		Debug.Log(score);
	}
}
